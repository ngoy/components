import PropTypes from 'prop-types';

const Button = ({
	children,
	primary,
	secondary,
	success,
	warning,
	danger,
	outline,
	rounded,
}) => {
	return <button>{children}</button>;
};

Button.propTypes = {
	checkVariationValue: ({ primary, secondary, success, danger, warning }) => {
		const count =
			Number(!!primary) +
			Number(!!secondary) +
			Number(!!success) +
			Number(!!danger) +
			Number(!!warning);

		if (count > 1) {
			return new Error(
				'Only one of primary, secondary, success, danger, warning can be true'
			);
		}
	},
};

export default Button;
