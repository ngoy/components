import Button from './Button';

function App() {
	return (
		<div>
			<div>
				<Button primary success rounded outline>
					Hi there!!
				</Button>
			</div>
			<div>
				<Button danger outline>
					Buy Now!
				</Button>
			</div>
			<div>
				<Button warning>See Deals!</Button>
			</div>
			<div>
				<Button>Hide Ads!</Button>
			</div>
			<div>
				<Button>Something!</Button>
			</div>
		</div>
	);
}

export default App;
